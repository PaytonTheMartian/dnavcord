# Dnavcord

Discord integration for Dnav, not much else to say.

## Installation

Download this repo, then copy it into your `Mods` folder.

Note that Dnavcord will probably only work on 64-bit machines, because the Discord Game SDK won't let me have both x86, x64, and aarch libraries in the same folders without a lot of work (they have the same filenames for some reason...).

Either way, to use this mod on a 32-bit/aarch machine then you would need to clone this repo, download the Discord Game SDK, then copy the 32-bit/aarch libraries into the `build` directory (also delete the old libraries. Leave `mod.dll` in the folder, that's the mod itself).
